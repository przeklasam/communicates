'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Created by Archeion-SR1 on 09.08.2017.
 */

/**
 *
 * @param place    -   miejsce wyswietlania się komunikatu
 * @param time      -   czas wyświetlania się komunikatu
 * @param queue     -   kolejka w jakiej znajdują sie komunikatu
 */

var Communicates = function () {
    function Communicates() {
        var place = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "#communicate-container";
        var time = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
        var queue = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'global';

        _classCallCheck(this, Communicates);

        if (!$('body').find(place)) this._place = place;else this._place = '#communicate-container';
        this._time = time;
        this._queue = queue;
    }

    _createClass(Communicates, [{
        key: 'addMessage',
        value: function addMessage(message) {
            var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'success';

            var notyfication;
            notyfication = new Noty({
                theme: 'bootstrap-v3',
                type: type,
                text: message,
                progressBar: false,
                timeout: this.time,
                container: this.place,
                queue: this.queue,
                closeWith: 'button',
                buttons: [Noty.button('X', 'btn-exit ' + type, function () {
                    notyfication.close();
                }.bind(this))],
                callbacks: {
                    afterShow: function () {
                        if (this.time) {
                            var timeout = this.time;
                            var text = notyfication.options.text;
                            setInterval(function () {
                                var newText = text;
                                timeout -= 1000;
                                var time = timeout / 1000;
                                newText += '<div class="timer">' + time + 's</div>';
                                notyfication.setText(newText);
                            }, 1000);
                        }
                    }.bind(this)
                }
            }).show();
        }
    }, {
        key: 'addSuccessMessage',
        value: function addSuccessMessage(message) {
            this.addMessage(message);
        }
    }, {
        key: 'addErrorMessage',
        value: function addErrorMessage(message) {
            this.addMessage(message, 'error');
        }
    }, {
        key: 'addInfoMessage',
        value: function addInfoMessage(message) {
            this.addMessage(message, 'info');
        }
    }, {
        key: 'CloseNotyfication',
        value: function CloseNotyfication() {
            Noty.closeAll(this.queue);
        }
    }, {
        key: 'place',
        set: function set(place) {
            this._place = place;
        },
        get: function get() {
            return this._place;
        }
    }, {
        key: 'time',
        set: function set(time) {
            this._time = time;
        },
        get: function get() {
            return this._time;
        }
    }, {
        key: 'queue',
        set: function set(queue) {
            this._queue = queue;
        },
        get: function get() {
            return this._queue;
        }
    }]);

    return Communicates;
}();

$(document).ready(function () {
    var test = new Communicates(undefined, 7500);

    setInterval(function () {
        test.addSuccessMessage('<b>testowa wiadomosc</b>');
        test.addErrorMessage('<b>testowa wiadomosc</b>');
    }, 5000);

    var newD = new Communicates("#zly-container", 10000);
    newD.addSuccessMessage('nowy w innym containerze');

    {}
});
