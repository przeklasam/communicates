/**
 * Created by Archeion-SR1 on 09.08.2017.
 */

/**
 *
 * @param place    -   miejsce wyswietlania się komunikatu
 * @param time      -   czas wyświetlania się komunikatu
 * @param queue     -   kolejka w jakiej znajdują sie komunikatu
 */

class Communicates {
    constructor(place = "#communicate-container", time = false, queue = 'global') {
        if(!$('body').find(place))
            this._place = place;
        else
            this._place = '#communicate-container';
        this._time = time;
        this._queue = queue;
    }

    set place(place) {
        this._place = place;
    }

    get place() {
        return this._place;
    }

    set time(time) {
        this._time = time;
    }

    get time() {
        return this._time;
    }

    set queue(queue) {
        this._queue = queue;
    }

    get queue() {
        return this._queue;
    }

    addMessage(message, type = 'success') {
        var notyfication;
        notyfication = new Noty({
            theme: 'bootstrap-v3',
            type: type,
            text: message,
            progressBar: false,
            timeout: this.time,
            container: this.place,
            queue: this.queue,
            closeWith: 'button',
            buttons: [
                Noty.button('X', `btn-exit ${type}`, function () {
                    notyfication.close();
                }.bind(this))
            ],
            callbacks: {
                afterShow: function(){
                    if(this.time)
                    {
                        let timeout = this.time;
                        let text = notyfication.options.text;
                        setInterval(function(){
                            let newText = text;
                            timeout -= 1000;
                            let time = timeout / 1000;
                            newText += `<div class="timer">${time}s</div>`;
                            notyfication.setText(newText);
                        },1000);
                    }
                }.bind(this)
            }
        }).show();
    }


    addSuccessMessage(message) {
        this.addMessage(message);
    }

    addErrorMessage(message) {
        this.addMessage(message, 'error');
    }

    addInfoMessage(message) {
        this.addMessage(message, 'info');
    }

    CloseNotyfication()
    {
        Noty.closeAll(this.queue);
    }

}

$(document).ready(function(){
    let test = new Communicates(undefined , 7500);

    setInterval(function(){
        test.addSuccessMessage('<b>testowa wiadomosc</b>');
        test.addErrorMessage('<b>testowa wiadomosc</b>');
    }, 5000)


    let newD = new Communicates("#zly-container", 10000);
    newD.addSuccessMessage('nowy w innym containerze');

    {

    }
});
